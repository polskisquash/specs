# Pobierz listę aktywnych rankingów

Zwraca listę aktywnych rankingów z informacją o najnowszym notowaniu.

**URL** : `/api/rankings/`

**Metoda** : `GET`

**Wymagana autoryzacja** : NIE

**Wymagany poziom uprawnień** : Brak

**Wymagane parametry** : `Brak`

## Udane wywołanie

**Wynik** : Użytkownik otrzymuje listę dostępnych rankingów.

**Kod** : `200 OK`

**Odpowiedź** : 
```json
[
    {
        "id" : 1,
        "name" : "OPEN",
        "description" : "Admin defined description of this ranking and its rules",
        "last_ranking_list" : { "id": 5544, "date" : "2019-08-20" }
    },
    {
        "id" : 10,
        "name" : "M40+",
        "description" : "Admin defined description of this ranking and its rules",
        "last_ranking_list" : { "id": 5549, "date" : "2019-08-20" }
    }
]
```
