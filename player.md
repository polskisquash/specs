# Pobierz informacje o zawodniku


**URL** : `/api/player/:id`

**Metoda** : `GET`

**Wymagana autoryzacja** : NIE

**Wymagany poziom uprawnień** : Brak

**Wymagane parametry** : `{ "id" : "000001" }`
  - **id** : numer licencji

## Udane wywołanie

**Wynik** : Użytkownik otrzymuje dane o zawodniku.

**Kod** : `200 OK`

**Odpowiedź** : 
```json
    {
        "id" : "000001",
        "first_name" : "John",
        "last_name" : "Snow",
        "gender" : "M|F|O",
        "rankings" : [
            { "id" : 1, "name" : "OPEN", "current_ranking" : 100, "best_ranking" : { "date" : "2018-01-01", "ranking" : 99 } }
        ]
    }
```
