openapi: 3.0.0
info:
  title: PZSQ API
  description: Definicja API umożliwiającego dostęp do danych Polskiego Związku Squasha
  version: 1.0.0
servers:
  - url: 'https://api.polskisquash.pl/'
security:
  - ApiKeyAuth: []
components:
  responses:
    UnauthorizedError:
      description: API Key is not valid for the requested resource
    ForbiddenError:
      description: Access denied
    UnexpectedError:
      description: An unexpected server error occurred
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: X-PZSQ-API-Key
  schemas:
    Event:
      type: object
      properties:
        id:
          type: string
        type:
          type: string
          enum: [ TOURNAMENT, PLS, MIGRATION ]
        start_date:
          type: string
          format: date
        end_date:
          type: string
          format: date
        rank:
          type: string
          enum: [A, B, B+, C, C+, IMP, MTOUR, MTOUR+, MB, MB+, IMPM, LIGA, PJO+, ZERO, DMP, DMPJ ]
        venues:
          type: array
          items:
            $ref: '#/components/schemas/Venue'
        officials:
          type: array
          items:
            allOf:
              - $ref: '#/components/schemas/Person'
              - type: object
                properties:
                  role:
                    type: string
                    enum: [ REFEREE, HEADREFEREE, DIRECTOR ]
        tournaments:
          type: array
          items:
            $ref: '#/components/schemas/Competition'
    Rules:
      type: object
      properties:
        matches:
          type: integer
        games:
          type: integer
    Competition:
      type: object
      properties:
        id:
          type: string
        roster:
          type: array
          items:
            $ref: '#/components/schemas/CompetitionEntry'
        standings:
          type: array
          items:
            $ref: '#/components/schemas/FinalStandingsEntry'
        infractions:
          type: array
          items:
            $ref: '#/components/schemas/Infraction'

    Infraction:
      type: object
      properties:
        person_id:
          type: string
        infraction_type:
          type: string
          enum: [ ABSENCE, WALKOVER ]
        details:
          type: string

    CompetitionRound:
      type: object
      properties:
        id:
          type: string
        type:
          type: string
          enum: [ MONRAD, ROUNDROBIN ]
        size:
          type: integer
        place:
          type: integer
        promote_to:
          type: array
          items:
            type: integer
        relegate_to:
          type: array
          items:
            type: integer
        roster:
          type: array
          items:
            type: integer
        fixtures:
          type: array
          items:
            $ref: '#/components/schemas/Fixture'
    CompetitionEntry:
      type: object
      allOf:
        - $ref: '#/components/schemas/Person'
        - type: object
          properties:
            time_of_entry:
              type: string
              format: date-time
            status:
              type: string
              enum: [ REGISTERED, CONFIRMED, RESERVE, RESIGNED, REJECTED ]
            ranking_position:
              type: integer
            seed:
              type: integer
    Venue:
      type: object
      properties:
        id:
          type: string
        name:
          type: string
        website:
          type: string
        city:
          type: string
        address:
          type: string
        contact:
          type: string
        courts:
          type: array
          items:
            type: object
            properties:
              id:
                type: string
              name:
                type: string
              type:
                type: string
                enum: [ REGULAR, GLASS ]
    Person:
      type: object
      required:
        - id
      properties:
        id:
          type: string
        first_name:
          type: string
        last_name:
          type: string
        gender:
          type: string
        date_of_birth:
          type: string
          format: date
        contact:
          type: string
    Team:
      type: object
      properties:
        id:
          type: string
    Schedule:
      type: object
      properties:
        court_id:
          type: integer
        planned_start_time:
          type: string
          format: date-time
        actual_start_time:
          type: string
          format: date-time
        total_duration:
          type: integer
        game_duration:
          type: array
          items:
            type: integer
    Fixture:
      type: object
      properties:
        id:
          type: string
        match_slot_number:
          type: integer
        playerA_id:
          type: integer
        playerB_id:
          type: integer
        referee_id:
          type: integer
        matches:
          type: array
          items:
            $ref: '#/components/schemas/Fixture'
        result:
          $ref: '#/components/schemas/Result'

    PartialResult:
      type: object
      properties:
        status:
          type: string
          enum: [COMPLETE, INJURY, ABSENCE]
        outcome:
          type: string
          enum: [ WIN, DRAW, LOSS, WALKOVER ]
        score:
          type: integer
        games:
          type: array
          items:
            type: integer

    Result:
      type: object
      properties:
        id:
          type: string
        fixture_id:
          type: string
        A:
          $ref: '#/components/schemas/PartialResult'
        B:
          $ref: '#/components/schemas/PartialResult'

    FinalStandings:
      type: object
      properties:
        id:
          type: string
        standings:
          type: array
          items:
            $ref: '#/components/schemas/FinalStandingsEntry'

    FinalStandingsEntry:
      type: object
      properties:
        player_id:
          type: integer
        place:
          type: integer


    RankingListEntry:
      type: object
      properties:
        player_id:
          type: string
        ranking:
          type: integer
        points:
          type: number
        first_name:
          type: string
        last_name:
          type: string
    RankingList:
      allOf:
        - $ref: '#/components/schemas/Ranking'
        - type: object
          properties:
            list_id:
              type: integer
              format: int64
            players:
              description: Number of players in this ranking list
              type: integer
            date:
              description: Date on which this list was generated
              type: string
            list:
              type: array
              items:
                $ref: '#/components/schemas/RankingListEntry'
    Ranking:
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
        description:
          type: string
        most_recent_list:
          $ref: '#/components/schemas/RankingList'
        recent_lists:
          type: array
          items:
            $ref: '#/components/schemas/RankingList'
paths:
  /ping:
    get:
      summary: Check if the server is running
      responses:
        '200':
          description: Server is up and running, API key is valid
        '401':
          $ref: "#/components/responses/UnauthorizedError"
        default:
          description: Unexpected error, server may be down
  /rankings:
    get:
      summary: List of all active rankings with corresponding most recent ranking lists
      responses:
        '200':
          description: Successful call
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Ranking'
        '401':
          $ref: "#/components/responses/UnauthorizedError"
        '403':
          $ref: "#/components/responses/ForbiddenError"
        default:
          $ref: "#/components/responses/UnexpectedError"
  /ranking/{id}:
    get:
      summary: Get ranking by id with recent lists data
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: Ranking ID
      responses:
        '200':
          description: Successful call
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Ranking'
        '401':
          $ref: "#/components/responses/UnauthorizedError"
        '403':
          $ref: "#/components/responses/ForbiddenError"
        default:
          $ref: "#/components/responses/UnexpectedError"
  /rankinglist/{listid}:
    get:
      summary: Get full ranking list by id
      parameters:
        - in: path
          name: listid
          schema:
            type: integer
          required: true
          description: Ranking list ID
      responses:
        '200':
          description: Successful call
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RankingList'
        '401':
          $ref: "#/components/responses/UnauthorizedError"
        '403':
          $ref: "#/components/responses/ForbiddenError"
        default:
          $ref: "#/components/responses/UnexpectedError"
  /event/{id}:
    get:
      summary: Get event details
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: Event ID
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
    post:
      summary: Upload the post-event report
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: Event ID
      requestBody:
        description: Report contents
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Event'

      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'

  /competition/{id}:
    get:
      summary: Get competition details
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: Competition ID
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Competition'


