# Struktury danych

## Zawodnik (Player)

 - **id** : string
 - **first_name** : string
 - **last_name** : string
 - **gender** : string enum of { MALE, FEMALE, OTHER }
 - date_of_birth : date
 - nationality : string (ISO 3166-1 alpha-3)
 - contact : string
 - city : string
 - club : string

## Uczestnik turnieju (TournamentEntry)

 - **inherits Player**
 - time_of_entry : datetime
 - ranking_position : integer
 - seed : integer
 - **status** : string enum of { REGISTERED, CONFIRMED, RESERVE, RESIGNED, REJECTED }
 - **role** : string enum of { PLAYER, REFEREE, HEAD_REFEREE, DIRECTOR }

## Drużyna (Team)

 - **id** : string
 - **name** : string
 - **city** : string
 - **country** : string (ISO 3166-1 alpha-3)
 - **captain** : object of Player
 - **owner** : object of Player
 - **roster** : list of TournamentEntry

## Turniej (Tournament)

 - **id** : string
 - **name** : string
 - **ranking** : string
 - **grade** : string
 - level : string
 - location : string
 - date : date
 - roster : list of TournamentEntry
 - rounds : list of TournamentRound
 - standings : list of FinalStandings

## Runda (TournamentRound)

 - **id** : string
 - name : string
 - type : string enum of { KNOCKOUT, ROUNDROBIN }
 - **size** : integer
 - **top_place** : integer
 - **promote_to** : TournamentRound->id
 - **relegate_to** : TournamentRound->id
 - rules : object of MatchRules
 - roster : list of TournamentEntry->id
 - draw : list of Fixture

## Mecz (Fixture)

 - **id** : string
 - match_slot_number : integer
 - **playerA_id** : TournamentEntry->id
 - **playerB_id** : TournamentEntry->id
 -  : list of Fixture
 - **referee** : TournamentEntry->id
 - result : object of Result
 - matches : list of Fixture
 - schedule : object of Schedule

## Wynik meczu (Result)

 - **fixture_id** : Fixture->id
 - A : object of 
    - status : string enum of { COMPLETE, INJURY, ABSENCE }
    - outcome : string enum of { WIN, DRAW, LOSS, WALKOVER }
    - score : integer
    - games : array of integer
 - B : .^.

## Protokół końcowy (FinalStandings)

## Ranking


