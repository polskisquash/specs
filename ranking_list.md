# Pobierz notowanie rankingu


**URL** : `/api/ranking/list/:listid`

**Metoda** : `GET`

**Wymagana autoryzacja** : NIE

**Wymagany poziom uprawnień** : Brak

**Wymagane parametry** : `{ "listid" : 1225 }`
  - **listid** : identyfikator notowania rankingu

## Udane wywołanie

**Wynik** : Użytkownik otrzymuje listę zawodników z ich pozycjami w podanym notowaniu rankingu.

**Kod** : `200 OK`

**Odpowiedź** : 
```json
    {
        "id" : 1225,
        "ranking_id" : 1,
        "ranking_name" : "OPEN",
        "date" : "2019-01-01",
        "players" : 2,
        "list" : [
            { "id" : "000001", "first_name" : "John", "last_name" : "Snow", "ranking" : 1, "points" : 1267.8 },
            { "id" : "000002", "first_name" : "Paul", "last_name" : "Kowalsky", "ranking" : 2, "points" : 1221.1 }
        ]
    }
```
