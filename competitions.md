# Pobierz listę wydarzeń rankingowych

Zwraca listę wydarzeń rankingowych (turniejów, lig, weryfikacji zawodników itp.), które odbyły się w ciągu 24 miesięcy od zadanej daty.

**URL** : `/api/competitions/:date0?`

**Metoda** : `GET`

**Wymagana autoryzacja** : NIE

**Wymagany poziom uprawnień** : Brak

**Parametry** : 

 - **date0** (opcjonalny) : lista zwróconych wydarzeń będzie zawierała wydarzenia które odbyły się w ciągu 24 miesięcy od tej daty

Jeżeli parametr jest pusty, zapytanie zwróci listę wydarzeń z ostatnich 24 miesięcy, według aktualnej daty.

## Udane wywołanie

**Wynik** : Użytkownik otrzymuje listę wydarzeń rankingowych.

**Kod** : `200 OK`

**Odpowiedź** : 

Definicja pól w obiektach "tournaments":

  - **type** : 
    - TOURNAMENT - turniej
    - TEAM - rozgrywki drużynowe
    - TEAMFINALS - finały DMP
    - ADMIN - wydarzenie wirtualne wynikające z przenoszenia zawodników między kategoriami wiekowymi
    - ZERO - dyscyplinarne zera
  - **rank** : ranga turnieju, wartości zgodne z RRI (C,C+,B,B+,A,IMP,MTOUR,MTOUR+,MB,MB+)
  - **level** : poziom turnieju, wartości zgodne z RRI (C0,C1,B0,B1,B2,B3,B4,B5,A,IMP,MTOUR,MTOUR+,MB0,MB1,MB2,MB3)

```json
[
    {
        "id" : 1000,
        "name" : "Turniej urodzinowy klubu X",
        "start_date" : "2018-01-01",
        "end_date" : "2018-01-02",
        "location" : { "id" : 999, "name" : "Klub X", "address" : "ul. Xowa 1, 01-001 Igrekowo" },
        "tournaments" : [ 
            { 
              "id" : 1567, 
              "type" : "TOURNAMENT", 
              "rank" : "B+",
              "level" : "B3",
              "rankings" : [ { "id" : 1, "name" : "OPEN" } ],
              "standings" : [
                  { "player" : "000001", "place" : 1, "points" : 185 },
                  { "player" : "000002", "place" : 2, "points" : 155 },
                  { "player" : "001027", "place" : 3, "points" : 133 },
              ]
            },
            { 
              "id" : 1568, 
              "type" : "TOURNAMENT", 
              "rank" : "B+",
              "level" : "B3",
              "rankings" : [ { "id" : 2, "name" : "DAMSKI OPEN" } ],
              "standings" : [
                  { "player" : "000001", "place" : 1, "points" : 185 },
                  { "player" : "000002", "place" : 2, "points" : 155 },
                  { "player" : "001027", "place" : 3, "points" : 133 }
              ]
            }            
        ]
    },
    {
        "id" : 1002,
        "name" : "Turniej finałowy klubu Y",
        "start_date" : "2018-02-01",
        "end_date" : "2018-02-02",
        "location" : { "id" : 179, "name" : "Klub Y", "address" : "ul. Yowa 1, 01-001 Iksowo" },
        "tournaments" : [ 
            { 
              "id" : 1571,
              "type" : "TOURNAMENT", 
              "rank" : "C",
              "level" : "C1",
              "rankings" : [ { "id" : 1, "name" : "OPEN" } ],
              "standings" : [
                  { "player" : "012345", "place" : 1, "points" : 140 },
                  { "player" : "005345", "place" : 2, "points" : 125 },
                  { "player" : "001434", "place" : 3, "points" : 99 }
              ]              
            }           
        ]
    }
]
```
