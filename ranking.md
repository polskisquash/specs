# Pobierz szczegółowe informacje o rankingu

Zwraca listę aktywnych rankingów z informacją o najnowszym notowaniu.

**URL** : `/api/ranking/:id`

**Metoda** : `GET`

**Wymagana autoryzacja** : NIE

**Wymagany poziom uprawnień** : Brak

**Wymagane parametry** : 

  - **id** : identyfikator rankingu

## Udane wywołanie GET

**Wynik** : Użytkownik informacje o rankingu.

**Kod** : `200 OK`

**Odpowiedź** : 
```json
{
    "id" : 1,
    "name" : "OPEN",
    "description" : "Admin defined description of this ranking and its rules",
    "last_ranking_list" : { "id": 5544, "date" : "2019-08-20" },
    "ranking_lists" : [
        { "id": 5544, "date" : "2019-08-20" },
        { "id": 5566, "date" : "2018-08-27" },
        { "id": 5577, "date" : "2018-09-03" }
    ]
},
```
