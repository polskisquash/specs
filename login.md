# Logowanie do API

**URL** : `/api/login`

**Metoda** : `POST`

**Wymagane parametry** : `{ "username" : "user1234", "password" : "password1234" }`
  - **username** : nazwa użytkownika nadana przez PZSQ
  - **password** : hasło

## Udane wywołanie

**Wynik** : Użytkownik identyfikator sesji

**Kod** : `200 OK`

**Odpowiedź** :
```json
    {
        "session_id" : "123123123123123123123123",
    }
```

## Kody błędu
 - **401** : błąd logowania, niepoprawne wartości parametrów **username**, lub **password**.

# Przekazywanie identfikatora sesji przy wywołaniach API

Po udanym logowaniu do API identyfikator sesji musi być przekazywany w nagłówku protokołu HTTP `X-Auth-PZSQ-API`.

# Zakończenie sesji

**URL** : `/api/logout`

**Metoda** : `GET`


