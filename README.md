# Propozycja specyfikacji formatu danych API

W tym repozytorium znajduje się projekt formatu danych służącego do przekazywania
należących do PZSQ danych rankingowych zewnętrznym użytkownikom.

- [Lista dostępnych rankingów](./rankings.md)
- [Szczegółowe informacje o rankingu](./ranking.md)
- [Notowania rankingów](./ranking_list.md)
- [Wydarzenia rankingowe i klasyfikacja końcowa](./competitions.md)
- [Dane zawodników](./player.md)

## Informacje techniczne

Wszystkie zapytania zwracają dane w formacie JSON. W przypadku udanego zapytania serwer zwraca kod **200** i w dane w treści odpowiedzi.

W przypadku błędu po stronie klienta serwer zwróci kod błędu z zakresu **400-499**, a w treść zawierać będzie szczegółowy opis błędu w formacie JSON:

```json
{ 
   "error" : "Error name",
   "description" : "Detailed error description"
}
```

W przypadku błędu po stronie serwera serwer zwróci kod błędu z zakresu **500-599**, a w treść zawierać będzie krótki opis błędu, formacie **text/plain**.
np. `500 Internal Server Error`




